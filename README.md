
## 3. JavaScript

> 3.1 - [Data types](./3_java_script/1_data-types.md)  
> 3.2 - [Operators, control flow and loops](./3_java_script/2_operators_control_loops.md)  
> 3.3 - [Objects](./3_java_script/3_objects.md)  
> 3.4 - [Functions](./3_java_script/4_functions.md)  
> 3.5 - [Arrays](./3_java_script/5_arrays.md)  
> 3.6 - [Error handling](./3_java_script/6_error_handling.md)  
> 3.7 - [Async programming](./3_java_script/7_async_programing.md)  
> 3.8 - [ES6 features](./3_java_script/8_es6_features.md)  
> 3.9 - [Typescript](./3_java_script/9_type_script.md)  

---

