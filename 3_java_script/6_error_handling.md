
<div align="center">
  <h2>3.6 Error handling</h2>
</div>

---

<code style="color : Red">?</code>
`#RRGGBB` 

### * 3.6.1*
> ### How to handle errors in JS?
> 
> `Grade: ?` `ET: ?`
> 
> <details>
> <summary>
> <b>Answer</b>
> </summary>
>
> > In JS we handle errors using the `try`...`catch` statements.
> 
> </details>
>
> ---
> 
> <details>
> <summary>
> <b>References</b>
> </summary>
>
> > [MDN: Exception handling statements](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Control_flow_and_error_handling#exception_handling_statements)
>
> </details>

---

### *? 3.6.2*
> ### How can we intentionally propagate an error?  Why would we need to do so? Provide an example.
>
> `Grade: ?` `ET: ?`
>
> <details>
> <summary>
> <b>Answer</b>
> </summary>
>
> > ?
> 
> </details>
>
> ---
>
> <details>
> <summary>
> <b>References</b>
> </summary>
>
> > [MDN: Throw Statement](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Control_flow_and_error_handling#throw_statement)
>
> </details>

---

### *? 3.6.3*
> ### How does `finally` block work?
>
> `Grade: ?` `ET: ?`
>
> <details>
> <summary>
> <b>Answer</b>
> </summary>
>
> > The `finally` block contains statements to be executed after the `try` and `catch` blocks execute.  
> > Additionally, the `finally` block executes before the code that follows the `try`…`catch`…`finally` statement.
>
> </details>
>
> ---
>
> <details>
> <summary>
> <b>References</b>
> </summary>
>
> > [MDN: try...catch statement](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Control_flow_and_error_handling#try...catch_statement)
>
> </details>

---

### *? 3.6.4*
> ### Is it possible to nest `try`...`catch` blocks?
>
> `Grade: ?` `ET: ?`
>
> <details>
> <summary>
> <b>Answer</b>
> </summary>
>
> > You can nest one or more `try`...`catch` statements.
> >
> > If an inner `try` block does not have a corresponding `catch` block:
> >
> > 1. it must contain a `finally` block, and
> > 2. the enclosing `try`...`catch` statement's `catch` block is checked for a match.
> 
> </details>
>
> ---
>
> <details>
> <summary>
> <b>References</b>
> </summary>
>
> > [MDN: nested try blocks](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/try...catch#nested_try_blocks)
>
> </details>