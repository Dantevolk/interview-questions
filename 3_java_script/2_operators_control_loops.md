
<div align="center">
  <h2>3.2 Operators, control flow and loops</h2>
</div>

---

### *3.2.1*
> ### What is the difference between double equals `==` and triple equals `===` ?
>
> `Grade: T1` `ET: 2 min`
>
> <details>
> <summary>
> <b>Answer</b>
> </summary>
>
> > Double equals `==` -  loose equality - perform a type conversion when comparing two things.
> >
> > Triple equals `===` will do the same comparison as double equals, but without type conversion; if the types differ, `false` is returned.
>
> </details>
>
> ---
>
> <details>
> <summary>
> <b>References</b>
> </summary>
>
> > [ECMAScript: Equality Operators](https://tc39.es/ecma262/#sec-equality-operators)  
> > [MDN: Equality comparisons and sameness](https://tc39.es/ecma262/#sec-equality-operators)
>
> </details>

---

### *3.2.2*
> ### How does Addition assignment `+=` (or any other equivalent) work?
>
> `Grade: T1` `ET: 1 min`
>
> <details>
> <summary>
> <b>Answer</b>
> </summary>
>
> > The addition assignment - performs addition (which is either numeric addition or string concatenation) on the two operands and assigns the result to the left operand.
>
> </details>
>
> ---
>
> <details>
> <summary>
> <b>References</b>
> </summary>
>
> > [MDN: Addition assignment](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Addition_assignment)
>
> </details>

---

### *3.2.3*
> ### How does Increment `++` or Decrement `--` works? What is the difference between their postfix and prefix syntax?
>
> `Grade: T1` `ET: 3 min`
>
> <details>
> <summary>
> <b>Answer</b>
> </summary>
>
> > The increment `++` operator increments (adds one to) its operand and returns the value before or after the increment, depending on where the operator is placed.
> >
> > If used postfix, with operator after operand (for example, `x++`), the increment operator increments and returns the value before incrementing.
> >
> > If used prefix, with operator before operand (for example, `++x`), the increment operator increments and returns the value after incrementing.
>
> </details>
>
> ---
>
> <details>
> <summary>
> <b>References</b>
> </summary>
>
> > [MDN: Increment / Decrement](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Increment)
>
> </details>

---

### *3.2.4*
> ### How does Nullish `??` coalescing operator works?
>
> `Grade: T2` `ET: 2 min`
>
> <details>
> <summary>
> <b>Answer</b>
> </summary>
>
> > The nullish coalescing `??` operator is a logical operator that returns its right-hand side operand when its left-hand side operand is `null` or `undefined`, and otherwise returns its left-hand side operand.
> 
> </details>
>
> ---
>
> <details>
> <summary>
> <b>References</b>
> </summary>
>
> > [MDN: Nullish](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Nullish_coalescing)
>
> </details>

---

### *3.2.5*
> ### How does Remainder `%` operator works?
>
> `Grade: T1` `ET: 1 min`
>
> <details>
> <summary>
> <b>Answer</b>
> </summary>
>
> > The remainder `%` operator returns the remainder left over when one operand is divided by a second operand. It always takes the sign of the dividend.
>
> </details>
>
> ---
>
> <details>
> <summary>
> <b>References</b>
> </summary>
>
> > [MDN: Remainder](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Remainder)
>
> </details>

---

### *3.2.6*
> ### What is the difference between Spread `...` & Rest `...` operator.
>
> `Grade: T2` `ET: 3 min`
>
> <details>
> <summary>
> <b>Answer</b>
> </summary>
>
> > Spread syntax looks exactly like rest syntax. In a way, spread syntax is the opposite of rest syntax. Spread syntax "expands" an array into its elements, while rest syntax collects multiple elements and "condenses" them into a single element.
>
> </details>
>
> ---
>
> <details>
> <summary>
> <b>References</b>
> </summary>
>
> > [MDN: Spread](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Spread_syntax)
>
> </details>

---

### *3.2.7*
> ### Please name logical operators that exist in JS.
>
> `Grade: T1` `ET: 1 min`
>
> <details>
> <summary>
> <b>Answer</b>
> </summary>
>
> > - Logical AND `&&`
> > - Logical NOT `!`
> > - Logical OR `||`
> >
> > New (optional):
> > - Logical OR assignment `||=`
> > - Logical AND assignment `&&=`
>
> </details>

---

### *3.2.8*
> ### Have you ever used bitwise operators? If yes, name some and explain how they work.
>
> `Grade: T4*` `ET: 3 min`
>
> <details>
> <summary>
> <b>Answer</b>
> </summary>
>
> > - Bitwise AND `&`
> > - Bitwise NOT `~`
> > - Bitwise OR `|`
> > - Bitwise XOR `^`
>
> </details>
>
> ---
>
> <details>
> <summary>
> <b>References</b>
> </summary>
>
> > - [MDN: Bitwise AND](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Bitwise_AND)
> > - [MDN: Bitwise NOT](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Bitwise_NOT)
> > - [MDN: Bitwise OR](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Bitwise_OR)
> > - [MDN: Bitwise XOR](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Bitwise_XOR)
>
> </details>

---

### *3.2.9*
> ### What iteration statements (loops) available in JS?
>
> `Grade: T1` `ET: 3 min`
>
> <details>
> <summary>
> <b>Answer</b>
> </summary>
>
> > - `for`
> > - `while`
> > - `do` ... `while`
> > - `for` ... `in`
> > - `for` ... `of`
> >
> > Optional:
> > - `labeled`
> > - `break`
> > - `continue`
>
> </details>
>
> ---
>
> <details>
> <summary>
> <b>References</b>
> </summary>
>
> > - [MDN: Loops & Iteration](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Loops_and_iteration)
>
> </details>

---

### *3.2.10*
> ### What is the difference between `break` and `continue` statement in loop?
>
> `Grade: T2` `ET: 2 min`
>
> <details>
> <summary>
> <b>Answer</b>
> </summary>
>
> > Break statement stops the entire process of the loop.
> > Continue statement only stops the current iteration of the loop.
> >
> > Break also terminates the remaining iterations.
> > Continue doesn't terminate the next iterations; it resumes with the successive iterations.
>
> </details>
>
> ---
>
> <details>
> <summary>
> <b>References</b>
> </summary>
>
> > - [MDN: Loops & Iteration](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Loops_and_iteration)
>
> </details>

---

### *3.2.11*
> ### What conditional statements JS supports?
>
> `Grade: T0` `ET: 2 min`
>
> <details>
> <summary>
> <b>Answer</b>
> </summary>
>
> > A conditional statement is a set of commands that executes if a specified condition is true. 
> > JavaScript supports two conditional statements: `if`...`else` and `switch`.
> 
> </details>
>
> ---
>
> <details>
> <summary>
> <b>References</b>
> </summary>
>
> > - [MDN: Control flow](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Control_flow_and_error_handling)
>
> </details>

---

### *3.2.12*
> ### How to handle exceptions in JS? How does `finally` block works? How to throw an error on purpose?
>
> `Grade: T0` `ET: 3 min`
>
> <details>
> <summary>
> <b>Answer</b>
> </summary>
>
> > You can throw exceptions using the `throw` statement and handle them using the `try`...`catch` statements.
> >
> > The `finally` block contains statements to be executed after the `try`...`catch` blocks execute.  
> > Additionally, the `finally` block executes before the code that follows the `try`...`catch`...`finally` statement.
>
> </details>
>
> ---
>
> <details>
> <summary>
> <b>References</b>
> </summary>
>
> > - [MDN: Error handling](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Control_flow_and_error_handling#exception_handling_statements)
>
> </details>

---
