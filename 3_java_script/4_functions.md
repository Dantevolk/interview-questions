
<div align="center">
  <h2>3.4 Functions</h2>
</div>

---

### *? 3.4.1*
> ### Function is an object?
>
> `Grade: ?` `ET: ? min`
>
> <details>
> <summary>
> <b>Answer</b>
> </summary>
>
> > ?
>
> </details>
>
> ---
>
> <details>
> <summary>
> <b>References</b>
> </summary>
>
> > ?
>
> </details>

---

### *? 3.4.2*
> ### What is the difference between function declaration and function expression?
>
> `Grade: ?` `ET: ? min`
>
> <details>
> <summary>
> <b>Answer</b>
> </summary>
>
> > ?
>
> </details>
>
> ---
>
> <details>
> <summary>
> <b>References</b>
> </summary>
>
> > [MDN: Function Declarations](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Functions#function_declarations)    
> > [MDN: Function Expressions](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Functions#function_expressions)
>
> </details>

---

### *? 3.4.3*
> ### What is closure?
>
> `Grade: ?` `ET: ? min`
>
> <details>
> <summary>
> <b>Answer</b>
> </summary>
>
> > ?
>
> </details>
>
> ---
>
> <details>
> <summary>
> <b>References</b>
> </summary>
>
> > [MDN: Closure](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Functions#closures)    
>
> </details>

---

### *? 3.4.4*
> ### How does arrow functions different from regular functions?
>
> `Grade: ?` `ET: ? min`
>
> <details>
> <summary>
> <b>Answer</b>
> </summary>
>
> > ?
>
> </details>
>
> ---
>
> <details>
> <summary>
> <b>References</b>
> </summary>
>
> > [MDN: Arrow Functions](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Functions#arrow_functions)
>
> </details>

---

### *? 3.4.5*
> ### What is Immediately Invoked Function Expression (IIFE)
>
> `Grade: ?` `ET: ? min`
>
> <details>
> <summary>
> <b>Answer</b>
> </summary>
>
> > ?
>
> </details>
>
> ---
>
> <details>
> <summary>
> <b>References</b>
> </summary>
>
> > [MDN: IIFE](https://developer.mozilla.org/en-US/docs/Glossary/IIFE)
>
> </details>

---

### *? 3.4.6*
> ### What type of scope do functions have?
>
> `Grade: ?` `ET: ? min`
>
> <details>
> <summary>
> <b>Answer</b>
> </summary>
>
> > ?
>
> </details>
>
> ---
>
> <details>
> <summary>
> <b>References</b>
> </summary>
>
> > [MDN: Function Scope](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Functions#function_scope)
>
> </details>

---

### *? 3.4.7*
> ### What are `getter` and `setter` functions?
>
> `Grade: ?` `ET: ? min`
>
> <details>
> <summary>
> <b>Answer</b>
> </summary>
>
> > ?
>
> </details>
>
> ---
>
> <details>
> <summary>
> <b>References</b>
> </summary>
>
> > [MDN: Getter & Setter functions](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Functions#getter_and_setter_functions)
>
> </details>

---

### *? 3.4.8*
> ### What does function return, if return statement is not explicitly declared? What about if it used with `new` operator?
>
> `Grade: ?` `ET: ? min`
>
> <details>
> <summary>
> <b>Answer</b>
> </summary>
>
> > By default it returns always `undefined`.  
> > If used with `new` - by default it will return `this` of current function, which is new created object of that function-constructor.
>
> </details>
>
> ---
>
> <details>
> <summary>
> <b>References</b>
> </summary>
>
> ?
>
> </details>

---

### *? 3.4.9*
> ### What tools do we have to override function scope? What are the difference between them?
>
> `Grade: ?` `ET: ? min`
>
> <details>
> <summary>
> <b>Answer</b>
> </summary>
>
> > `call`, `bind`, `apply`.
>
> </details>
>
> ---
>
> <details>
> <summary>
> <b>References</b>
> </summary>
>
> > [MDN: apply](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Function/apply)
> > [MDN: bind](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Function/bind)
> > [MDN: call](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Function/call)
>
> </details>

---

### *? 3.4.10*
> ### What are `generator` functions? What does they return? Provide an example of such function
>
> `Grade: ?` `ET: ? min`
>
> <details>
> <summary>
> <b>Answer</b>
> </summary>
>
> > Function that use asterics in their declaration, return `Generator` object, and use `yield` instead of `return`.
>```javascript
> const foo = function* () {
>   yield 'a';
>   yield 'b';
>   yield 'c';
> };
>```
> </details>
>
> ---
>
> <details>
> <summary>
> <b>References</b>
> </summary>
>
> > [MDN: Generator](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Generator)
>
> </details>

---

### *? 3.4.11*
> ### What are `async` functions? What they return? Provide an example.
>
> `Grade: ?` `ET: ? min`
>
> <details>
> <summary>
> <b>Answer</b>
> </summary>
>
> > Async functions are functions that always return a promise,  
> > and use can use await keyword within their scope. If the return value of an async function   
> > is not explicitly a promise, it will be implicitly wrapped in a promise.
>```javascript
> async function foo() {
>   return 1;
> }
>```
> </details>
>
> ---
>
> <details>
> <summary>
> <b>References</b>
> </summary>
>
> > [MDN: Async Function](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/async_function)
>
> </details>