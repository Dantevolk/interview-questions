
<div align="center">
  <h2>3.8 ES6 features</h2>
</div>

---

### *3.8.1*
> ### Name features that came with ES6.
>
> `Grade: T1` `ET: 5 min`
>
> <details>
> <summary>
> <b>Answer</b>
> </summary>
>
> > 1. let and const Keywords
> > 2. Arrow Functions
> > 3. Multi-line Strings
> > 4. Default Parameters
> > 5. Template Literals
> > 6. Destructuring Assignment
> > 7. Enhanced Object Literals
> > 8. Promises
> > 9. Classes
> > 10. Modules  
> > etc...
> 
> </details>
>
> ---
>
> <details>
> <summary>
> <b>References</b>
> </summary>
>
> </details>

---

### *3.8.2*
> ### What are the Template literals (Template strings)?
>
> `Grade: T1` `ET: 2 min`
>
> <details>
> <summary>
> <b>Answer</b>
> </summary>
>
> > Template literals are literals delimited with backtick (`) characters,   
> > allowing for multi-line strings, string interpolation with embedded expressions,  
> > and special constructs called tagged templates.
>
> </details>
>
> ---
>
> <details>
> <summary>
> <b>References</b>
> </summary>
>
> > [MDN: Template literals](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Template_literals)
>
> </details>

---

### *3.8.3*
> ### How does object/array Destructuring assignment work? Provide an example for both array & object.
>
> `Grade: T1` `ET: 3 min`
>
> <details>
> <summary>
> <b>Answer</b>
> </summary>
>
> > The destructuring assignment is an expression that makes it easy to extract values from arrays,  
> > or properties from objects, into distinct variables.
> ```javascript
>  //Array Destructuring
> let fruits = ["Apple", "Banana"];
> let [a, b] = fruits; // Array destructuring assignment
> console.log(a, b);
> 
> //Object Destructuring
> let person = {name: "Peter", age: 28};
> let {name, age} = person; // Object destructuring assignment
> console.log(name, age);
> ```
>
> </details>
>
> ---
>
> <details>
> <summary>
> <b>References</b>
> </summary>
>
> </details>

---

### *3.8.4*
> ### What benefits we received with ES6 modules?
>
> `Grade: T2` `ET: 3 min`
>
> <details>
> <summary>
> <b>Answer</b>
> </summary>
>
> > Previously, there was no native support for modules in JavaScript.  
> > ES6 introduced a new feature called modules, in which each module is represented   
> > by a separate ".js" file. We can use the "import" or "export" statement in a module  
> > to import or export variables, functions, classes or any other component from/to different files and modules.
> ```javascript
> // file a.js
> export const num = 50;
> export function getName(fullName) {   
>   //data
> };
> 
> // file b.js
> import {num, getName} from './a.js';
> console.log(num); // 50
> ```
>
> </details>
>
> ---
>
> <details>
> <summary>
> <b>References</b>
> </summary>
>
> </details>

---

### *3.8.5*
> ### How do ES6 classes simplifies our life compared to constructor functions?
>
> `Grade: T2` `ET: 5 min`
>
> <details>
> <summary>
> <b>Answer</b>
> </summary>
>
> > Previously, classes never existed in JavaScript.  
> > Classes are introduced in ES6 which looks similar to classes in other object-oriented languages,   
> > such as C++, Java, PHP, etc. But, they do not work exactly the same way.   
> > ES6 classes make it simpler to create objects, implement inheritance  
> > by using the "extends" keyword and also reuse the code efficiently.
> ```javascript
> class UserProfile {   
>   constructor(firstName, lastName) {
>       this.firstName = firstName;
>       this.lastName = lastName;     
>   } 
>
>   getName() {       
>       console.log(`The Full-Name is ${this.firstName} ${this.lastName}`);    
>   }
> }
> 
> let obj = new UserProfile('John', 'Smith');
> obj.getName(); // output: The Full-Name is John Smith
> ```
>
> </details>
>
> ---
>
> <details>
> <summary>
> <b>References</b>
> </summary>
>
> </details>
