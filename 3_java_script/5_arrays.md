
<div align="center">
  <h2>3.5 Arrays</h2>
</div>

---

### *3.5.1*
> ### What is an array?
>
> `Grade: T0` `ET: 2 min`
>
> <details>
> <summary>
> <b>Answer</b>
> </summary>
>
> > Arrays are generally described as "list-like objects"; they are basically single objects that contain  
> > multiple values stored in a list. Array objects can be stored in variables and dealt with in much the  
> > same way as any other type of value, the difference being that we can access each value inside the list individually,  
> > and do super useful and efficient things with the list, like loop through it and do the same thing to every value.
>
> </details>
>
> ---
>
> <details>
> <summary>
> <b>References</b>
> </summary>
>
> > [MDN: Array](https://developer.mozilla.org/en-US/docs/Learn/JavaScript/First_steps/Arrays#what_is_an_array)
>
> </details>

---

### *3.5.2*
> ### How to create an array?
>
> `Grade: T0` `ET: 1 min`
>
> <details>
> <summary>
> <b>Answer</b>
> </summary>
>
> > Arrays consist of square brackets and items that are separated by commas.  
> > ex: `const sequence = [1, 1, 2, 3, 5, 8, 13];`
>
> </details>
>
> ---
>
> <details>
> <summary>
> <b>References</b>
> </summary>
>
> > [MDN: Creating an array](https://developer.mozilla.org/en-US/docs/Learn/JavaScript/First_steps/Arrays#creating_arrays)
>
> </details>

---

### *3.5.3*
> ### How to find the length of an array?
>
> `Grade: T0` `ET: 1 min`
>
> <details>
> <summary>
> <b>Answer</b>
> </summary>
>
> > You can find out the length of an array (how many items are in it)  
> > in exactly the same way as you find out the length (in characters) of a string — by using the `length` property.
>
> </details>
>
> ---
>
> <details>
> <summary>
> <b>References</b>
> </summary>
>
> > [MDN: Array length](https://developer.mozilla.org/en-US/docs/Learn/JavaScript/First_steps/Arrays#finding_the_length_of_an_array)
>
> </details>

---

### *3.5.4*
> ### What options do we have to add/remove an item from an array?
>
> `Grade: T1` `ET: 2 min`
>
> <details>
> <summary>
> <b>Answer</b>
> </summary>
>
> > To add:
> > - `push()`
> > - `unshift()`
> > - directly by index. Ex: `arr[100] = 1;`
> >
> > To remove:
> > - `pop()`
> > - `shift()`
> > - `splice()`
>
> </details>
>
> ---
>
> <details>
> <summary>
> <b>References</b>
> </summary>
>
> > [MDN: Adding items](https://developer.mozilla.org/en-US/docs/Learn/JavaScript/First_steps/Arrays#adding_items)  
> > [MDN: Removing items](https://developer.mozilla.org/en-US/docs/Learn/JavaScript/First_steps/Arrays#removing_items)
>
> </details>

---

### *3.5.5*
> ### What operators/methods do you know to loop thru an array?
>
> `Grade: T2` `ET: 3 min`
>
> <details>
> <summary>
> <b>Answer</b>
> </summary>
>
> > - `for`
> > - `for... of`
> > - `while` & `do...while`
> > - `Array.prototype.map()`
> > - `Array.prototype.forEach()`
> > - `Array.prototype.filter()`
> > - `Array.prototype.reduce()`
>
> </details>
>
> ---
>
> <details>
> <summary>
> <b>References</b>
> </summary>
>
> </details>

---

### *3.5.6*
> ### How too traverse an array without using any of the loop operators, nor `Array.prototype` methods?
>
> `Grade: T3` `ET: 2 min`
>
> <details>
> <summary>
> <b>Answer</b>
> </summary>
>
> > We can achieve that using recursion.
>
> </details>
>
> ---
>
> <details>
> <summary>
> <b>References</b>
> </summary>
>
> </details>