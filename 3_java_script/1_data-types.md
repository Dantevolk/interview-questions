
<div align="center">
  <h1>3.1 Data Types</h1>
</div>

---

### *3.1.1*
> ### Name all data types that you know in JS.
>
> `Grade: T1` `ET: 1 min`
>
> <details>
> <summary>
> <b>Answer</b>
> </summary>
>
> > **Primitive data types**
> > - Boolean
> > - Number
> > - String
> > - Null
> > - undefined
> > - Symbol
> >
> > **Non-primitive data types (reference)**
> > - Object
>
> </details>
>
> ---
>
> <details>
> <summary>
> <b>References</b>
> </summary>
>
> > [ECMAScript Data types](https://tc39.es/ecma262/#sec-ecmascript-language-types)
>
> </details>

---

### *3.1.2*
> ### Does JS have `Function` data type?
>
> `Grade: T2` `ET: 1 min`
>
> <details>
> <summary>
> <b>Answer</b>
> </summary>
>
> > Nope. Function is a subclass of an Object. As a result it is an Object data type.
>
> </details>
>
> ---
>
> <details>
> <summary>
> <b>References</b>
> </summary>
>
> > [Function Prototype Object](https://tc39.es/ecma262/#sec-properties-of-the-function-prototype-object)
>
> </details>

---

### *3.1.3*
> ### Does JS have separate data type for an `Array`?
>
> `Grade: T2` `ET: 1 min`
>
> <details>
> <summary>
> <b>Answer</b>
> </summary>
>
> > Nope. Array is a subclass of an Object. As a result it is an Object data type.
>
> </details>
>
> ---
>
> <details>
> <summary>
> <b>References</b>
> </summary>
>
> > [Array Prototype Object](https://tc39.es/ecma262/#sec-properties-of-the-array-prototype-object)
>
> </details>

---

### *3.1.4*
> ### If you use typeof operator on the array it would return you and `"object"` string, why is that?
>
> `Grade: T4` `ET: 2 min`
>
> <details>
> <summary>
> <b>Answer</b>
> </summary>
>
> > That is the way typeof operator works with an `Array` and any other objects that does not implement `[[Call]]` internal method.
> > Exception is `Function` - it would return `"function"` string (since it implements `[[Call]]` method).
>
> </details>
>
> ---
>
> <details>
> <summary>
> <b>References</b>
> </summary>
>
> > [typeof operator](https://262.ecma-international.org/13.0/#sec-typeof-operator)  
> > [\[\[Call\]\]](https://262.ecma-international.org/13.0/#sec-typeof-operator)
>
> </details>

---

### *3.1.5*
> ### What is the difference between `null` & `undefined`?
>
> `Grade: T3` `ET: 1 min`
>
> <details>
> <summary>
> <b>Answer</b>
> </summary>
>
> > `null` - an empty or non-existent value. `null` is assigned, and explicitly means nothing.
> >
> > `undefined` -  a variable has been declared, but the value of that variable has not yet been defined.
>
> </details>

---

### *3.1.6*
> ### What are the falsy values in JavaScript??
>
> `Grade: T1` `ET: 2 min`
>
> <details>
> <summary>
> <b>Answer</b>
> </summary>
>
> > In JS, falsy values are the following:
> >
> > - `""` (empty string)
> > - `0`
> > - `null`
> > - `undefined`
> > - `false`
> > - `NaN` (not a number)
> >
> > They become `false` when they’re converted to a boolean value.
>
> </details>

---

### *3.1.7*
> ### What is the `Symbol` data type?
>
> `Grade: T3` `ET: 3 min`
>
> <details>
> <summary>
> <b>Answer</b>
> </summary>
>
> > The `Symbol` type is the set of all non-String values that may be used as the key of an `Object` property.
> >
> > Each possible `Symbol` value is unique and immutable.
> >
> > Each `Symbol` value immutably holds an associated value called `[[Description]]` that is either `undefined` or a `String` value.
>
> </details>
>
> ---
>
> <details>
> <summary>
> <b>References</b>
> </summary>
>
> > [The Symbol Type](https://tc39.es/ecma262/#sec-ecmascript-language-types-symbol-type)
>
> </details>

---

### *3.1.8*
> ### Can you please name some of the well-known `Symbols`?
>
> `Grade: T4` `ET: 2 min`
>
> <details>
> <summary>
> <b>Answer</b>
> </summary>
>
> > - `Symbol.asyncIterator`
> > - `Symbol.iterator`
> > - `Symbol.hasInstance`
> > - etc...
>
> </details>
>
> ---
>
> <details>
> <summary>
> <b>References</b>
> </summary>
>
> > [Well-Known Symbols](https://tc39.es/ecma262/#sec-well-known-symbols)
>
> </details>

---
