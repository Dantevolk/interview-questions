
<div align="center">
  <h2>3.3 Objects</h2>
</div>

---

### *3.3.1*
> ### What options do we have to create an `object` in JS?
>
> `Grade: T1` `ET: 3 min`
>
> <details>
> <summary>
> <b>Answer</b>
> </summary>
>
> > - `Object.create()`
> > - constructor function with `new` operator
> > - object initializer `{}`
>
> </details>
>
> ---
>
> <details>
> <summary>
> <b>References</b>
> </summary>
>
> > [MDN: Object Initializer](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Object_initializer)  
> > [MDN: new](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/new)  
> > [MDN: Object.create()](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/create)  
> > [MDN: Creating new objects](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Working_with_objects#creating_new_objects)
> 
> </details>

---

### *3.3.2*
> ### How to remove non-inherited property from an object?
>
> `Grade: T1` `ET: 3 min`
>
> <details>
> <summary>
> <b>Answer</b>
> </summary>
>
> > The `delete` operator removes a property from an object.  
> > If the property's value is an object and there are no more references to the object,  
> > the object held by that property is eventually released automatically.
>
> </details>
>
> ---
>
> <details>
> <summary>
> <b>References</b>
> </summary>
>
> > [MDN: Delete](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/delete)
>
> </details>
---

### *3.3.3*
> ### What are the ways to list/traverse object properties?
>
> `Grade: T2` `ET: 5 min`
>
> <details>
> <summary>
> <b>Answer</b>
> </summary>
>
> > There are three native ways to list/traverse object properties:
> >
> > - `for...in` loops - This method traverses all of the enumerable string   
> > properties of an object as well as its prototype chain.
> > - `Object.keys()` - This method returns an array with only the enumerable own string  
> > property names ("keys") in the object myObj, but not those in the prototype chain.
> > - `Object.getOwnPropertyNames()` - This method returns an array containing all the own   
> > string property names in the object myObj, regardless of if they are enumerable or not.
>
> </details>
>
> ---
>
> <details>
> <summary>
> <b>References</b>
> </summary>
>
> > [MDN: Object keys](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/keys)   
> > [MDN: Object.getOwnProperyNames()](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/getOwnPropertyNames)  
> > [MDN: for...in](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/for...in)  
>
> </details>

---

### *3.3.4*
> ### How does prototype inheritance works?
>
> `Grade: T3` `ET: 5 min`
>
> <details>
> <summary>
> <b>Answer</b>
> </summary>
>
> > JavaScript implements inheritance by using objects. Each object has an internal link to another object called its prototype.  
> > That prototype object has a prototype of its own, and so on until an object is reached with `null` as its prototype.   
> > By definition, `null` has no prototype and acts as the final link in this prototype chain.   
> > It is possible to mutate any member of the prototype chain or even swap out the prototype at runtime,  
> > so concepts like static dispatching do not exist in JavaScript.
>
> </details>
>
> ---
>
> <details>
> <summary>
> <b>References</b>
> </summary>
>
> > [MDN: Prototype inheritance](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Inheritance_and_the_prototype_chain)
>
> </details>

---

### *3.3.5*
> ### What is an object method? How to define a method for an object?
>
> `Grade: T0` `ET: 3 min`
>
> <details>
> <summary>
> <b>Answer</b>
> </summary>
>
> > A method is a function associated with an object, or, put differently,  
> > a method is a property of an object that is a function. Methods are defined the way   
> > normal functions are defined, except that they have to be assigned as the property of an object.
> 
> ```javascript
> objectName.methodName = functionName;
> 
> const myObj = {
>   myMethod: function (params) {
>     // do something
>   },
> 
>   // this works too!
>   myOtherMethod(params) {
>     // do something else
>   },
> };
> ```
> 
> </details>
>
> ---
>
> <details>
> <summary>
> <b>References</b>
> </summary>
>
> > [MDN: Defining methods](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Working_with_objects#defining_methods)
>
> </details>

---

### *3.3.6*
> ### What are object property getter and setter? Provide an example.
>
> `Grade: T1` `ET: 3 min`
>
> <details>
> <summary>
> <b>Answer</b>
> </summary>
>
> > A getter is a function associated with a property that gets the value of a specific property.  
> > A setter is a function associated with a property that sets the value of a specific property.   
> > Together, they can indirectly represent the value of a property.
> ```javascript
> const myObj = {
>   a: 7,
>   get b() {
>       return this.a + 1;
>   },
>   set c(x) {
>       this.a = x / 2;
>   }, 
> };
> ```
>
> </details>
>
> ---
>
> <details>
> <summary>
> <b>References</b>
> </summary>
>
> > [MDN: get](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Functions/get)  
> > [MDN: set](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Functions/set)
>
> </details>

---

### *3.3.7*
> ### What is the result of comparing equal objects using `==` and `===` operators.
>
> `Grade: T0` `ET: 2 min`
> ```javascript 
> // Two variables, two distinct objects with the same properties
> const fruit = { name: "apple" };
> const fruitbear = { name: "apple" };
> 
> fruit == fruitbear;
> fruit === fruitbear;
>```
> 
> <details>
> <summary>
> <b>Answer</b>
> </summary>
>
> > In JavaScript, objects are a reference type. Two distinct objects are never equal,   
> > even if they have the same properties. Only comparing the same object reference with itself yields true.
> ```javascript
> fruit == fruitbear; // return false
> fruit === fruitbear; // return false
> ```
>
> </details>
>
> ---
>
> <details>
> <summary>
> <b>References</b>
> </summary>
>
> > [MDN: Comapring object](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Working_with_objects#comparing_objects)
>
> </details>